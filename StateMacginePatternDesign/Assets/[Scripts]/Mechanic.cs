﻿using UnityEngine;

public class Mechanic : MonoBehaviour //context sınıfım
{
    [HideInInspector] public IMechanic tapMechanic;
    [HideInInspector] public IMechanic swipeMechanic;
    [HideInInspector] public IMechanic dragAndDropMechanic;
    private IMechanic m_mechanic;

    public Mechanic()
    {
        tapMechanic = new TapMechanic(this);
        swipeMechanic = new SwipeMechanic(this);
        dragAndDropMechanic = new DragAndDropMechanic(this);
        m_mechanic = swipeMechanic;
    }

    public void TapMechanic()
    {
        m_mechanic.Tap();
    }

    public void SwipeMechanic()
    {
        m_mechanic.Swipe();
    }

    public void DragAndDropMechanic()
    {
        m_mechanic.DragAndDrop();
    }

    public void SetMechanic(IMechanic mechanic)
    {
        this.m_mechanic = mechanic;
    }

    public IMechanic GetTapMechanic()
    {
        return tapMechanic;
    }

    public IMechanic GetSwipeMechanic()
    {
        return swipeMechanic;
    }

    public IMechanic GetDragAndDropMechanic()
    {
        return dragAndDropMechanic;
    }
}
