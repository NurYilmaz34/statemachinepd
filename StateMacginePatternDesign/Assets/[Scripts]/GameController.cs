﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private Mechanic mechanic;

    public Button TapButton;
    public Button SwipeButton;
    public Button DragandDropButton;

    private void Awake()
    {
        mechanic = new Mechanic();
    }

    private void Start()
    {
        mechanic.TapMechanic();
        mechanic.SwipeMechanic();
        mechanic.SetMechanic(mechanic.dragAndDropMechanic);
        mechanic.DragAndDropMechanic();
    }
}
