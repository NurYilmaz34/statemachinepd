﻿public interface IMechanic
{
    void Tap();
    void Swipe();
    void DragAndDrop();
}
